#! /bin/tcsh -f
# -*- Shell-script -*- Time-stamp: <2020-10-05 12:44:03 sander>

# this script must be called from the caaba directory:
# cd caaba
# ../git_prepare_caaba_commit.tcsh

# from xcaaba.py:
git checkout -- depend.mk
git checkout -- caaba.nml
git checkout -- nml/mecca_benchmark.nml

# from xmecca:
git checkout -- mecca/batch/benchmark.bat
git checkout -- mecca/graphviz/*.pdf
git checkout -- mecca/integr.kpp
git checkout -- mecca/latex/mecca_*.tex
git checkout -- mecca/latex/meccalit.bib
git checkout -- mecca/latex/meccanism.pdf
git checkout -- mecca/mecca.eqn
git checkout -- mecca/mecca.spc
git checkout -- mecca/smcl/messy_mecca_kpp*.f90
git checkout -- pycaaba/_mecca_spc.py
